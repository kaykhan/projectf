from api import API
from aiohttp import web

server = API()
web.run_app(server.app)
