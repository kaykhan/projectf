import React, { Component, Fragment } from 'react'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

interface IProps {
    users: IUser[];
}

interface IState {
}

class UserListComponent extends Component<IProps, IState> {

    constructor(props: any) {
        super(props);
        console.log(props);
    }

    renderUserlist = () => {

        let users;
        if (this.props.users) {
            users = this.props.users.map((u: IUser) => {
                return (
                    <Fragment>
                        <ListItem alignItems="flex-start" key={u.id}>
                            <ListItemAvatar>
                                <Avatar alt="Remy Sharp" src={u.avatar} />
                            </ListItemAvatar>
                            <ListItemText
                                primary={u.first_name + " " + u.last_name}
                                secondary={u.email}
                            />
                        </ListItem>
                        <Divider variant="inset" component="li" />
                    </Fragment>
                )
            });
        }

        return users;
    }

    render() {
        return (
            <div>
                <List>
                    {this.renderUserlist()}
                </List>
            </div>
        )
    }
}

interface IUser {
    id: number;
    email: string;
    first_name: string;
    last_name: string;
    avatar: string;
}

export default UserListComponent;