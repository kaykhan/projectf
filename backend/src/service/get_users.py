import aiohttp

async def get_users():
    async with aiohttp.ClientSession() as session:
        async with session.get('https://reqres.in/api/users') as resp:
            return await resp.json()