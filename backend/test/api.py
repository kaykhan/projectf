from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web

from src.api import API

class ApiTestCase(AioHTTPTestCase):

    async def get_application(self):
        server = API()
        app = server.app
        return app

    @unittest_run_loop
    async def test_health_check(self):
        resp = await self.client.request("GET", "/")
        assert resp.status == 200

    @unittest_run_loop
    async def test_list_users(self):
        resp = await self.client.request("GET", "/api/users")
        assert resp.status == 200
