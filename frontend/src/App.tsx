import React, { Component, Fragment } from "react";
import { GetUserList } from "./service/user.service";
import UserListComponent from "./component/UserList";

interface IProps { }

interface IState {
    users: any[];
}

class App extends Component<IProps, IState> {

    constructor(props: any) {
        super(props);

        this.state = {
            users: []
        };

    }

    public async componentDidMount() {

        const request = await GetUserList();

        if (request.status === 200) {
            this.setState({
                users: request.data.data.users
            });
        }
    }

    public renderUserListComponent() {
        
        const { users } = this.state;

        if (users && users.length > 0) {
            return (
                <UserListComponent users={users}></UserListComponent>
            )
        }

    }


    public render() {

        return (
            <Fragment>
                {this.renderUserListComponent()}
            </Fragment>
        );
    }
}

export default App;
