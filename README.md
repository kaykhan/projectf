# Project

## Deployment

### prerequisites
```
docker (https://docs.docker.com/install/)
docker-compose (https://docs.docker.com/compose/install/)
```
### Instructions 

1. cd projectf
2. docker-compose up

API now running on port 3000

Frontend now running on port 8080

## Development

### prerequisites

```
python
pipenv
```
### Instructions 

1. cd projectf/backend
2. pipenv shell
3. pipenv install
4. python src/server.py (start local server)
5. pytest test/api.py (run unit tests)

## Frontend

### prerequisites

```
node
npm
```
### Instructions 

1. cd projectf/frontend
2. npm install
3. npm run start


## Thoughts

This project is built with a microservice architecture in mind. The backend is an api built using python and the frontend is a single page application built using reactjs/node. 

Backend: The api is built using the asyncio/aiohttp library. A single api/users endpoint is exposed to return a list of users from a third party service. aiohttp is lightweight webserver compared to django and inherently asynchrnous (non-blocking) compared to flask.

Frontend: Reactjs is used to develop a single page application. It is decoupled from the backend. It simply interfaces with the backend api to retrieve user data and output that data to a page. Material-ui is used for the design.