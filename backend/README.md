# Backend

## Development Prerequisites

```
python
pipenv
```

## Install 

1. `pipenv shell`
2. `pipenv install`


## Development

`python src/server.py`

## Test

`pytest test/api.py`


## Deploy (Locally)

`docker-compose up`