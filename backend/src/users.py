import json
from aiohttp import web
from src.service.get_users import get_users

class UsersResource():

    def __init__(self):
        pass

    async def list_users(self, request):
        
        users = await get_users()
    
        response = {
            'status': 200,
            'data': {
                "users": users["data"]
            },
        }

        return web.json_response(data=response, status=response.get('status'))
