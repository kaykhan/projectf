import axios, { AxiosRequestConfig } from "axios";

export const GetUserList = () => {

    const url = process.env.REACT_APP_API_URL + "/api/users";

    const requestConfig: AxiosRequestConfig = {
        url,
        method: "GET",
    };

    return axios(requestConfig);

};
