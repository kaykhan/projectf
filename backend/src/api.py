import json
import os
import sys
from aiohttp import web
import aiohttp_cors

sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

from src.users import UsersResource

class API():

    def __init__(self):

        self.app = web.Application()
        
        cors = aiohttp_cors.setup(self.app, defaults={
            "*": aiohttp_cors.ResourceOptions(
                    allow_credentials=True,
                    expose_headers="*",
                    allow_headers="*",
                )
        })

        self.users_resource = UsersResource()

        self.app.add_routes([
            web.get('/', self.health_check),
            web.get("/api/users", self.users_resource.list_users)
        ])

        for route in list(self.app.router.routes()):
            cors.add(route)

    async def health_check(self,request):
        response = {
            'status': 200,
            'message': "Up",
        }

        return web.json_response(data=response, status=response.get('status'))
